package krzbb.com.androidbasics_navigation.infrastructure.ui

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import krzbb.com.androidbasics_navigation.R

object ImageViewBindingAdapters
{
    @BindingAdapter("imageUrl")
    @JvmStatic
    fun setLoadImage(view: ImageView, imageUrl: String?)
    {
        Glide.with(view.context)
                .load(imageUrl)
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .into(view)
    }
}