package krzbb.com.androidbasics_navigation.modules

import krzbb.com.androidbasics_navigation.details.ArticleViewModel
import krzbb.com.androidbasics_navigation.feed.data.datasource.FeedApi
import krzbb.com.androidbasics_navigation.feed.domain.FeedRepository
import krzbb.com.androidbasics_navigation.feed.domain.FeedRepositoryImpl
import krzbb.com.androidbasics_navigation.infrastructure.httpclient.createWebService
import krzbb.com.androidbasics_navigation.feed.ui.FeedViewModel
import krzbb.com.androidbasics_navigation.settings.SettingsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single { createWebService(FeedApi::class.java) }
    single<FeedRepository> { FeedRepositoryImpl(get(), get()) }

    viewModel { FeedViewModel(get()) }
    viewModel { SettingsViewModel() }
    viewModel { ArticleViewModel(get()) }

}