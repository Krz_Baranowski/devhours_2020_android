package krzbb.com.androidbasics_navigation.feed.data

data class FeedModel(
        val status: String,
        val totalResults: Long,
        val articles: List<ArticleModel>
)

data class ArticleModel(
        val source: FeedSourceModel,
        val author: String? = null,
        val title: String,
        val description: String?,
        val url: String?,
        val urlToImage: String?,
        val publishedAt: String,
        val content: String?,
        val articleUrl: String?
)

data class FeedSourceModel(
        val id: Any? = null,
        val name: String
)