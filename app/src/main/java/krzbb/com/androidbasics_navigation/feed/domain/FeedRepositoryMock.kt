package krzbb.com.androidbasics_navigation.feed.domain

import com.soywiz.klock.DateTime
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import krzbb.com.androidbasics_navigation.feed.domain.model.Article

class FeedRepositoryMock : FeedRepository
{
    private var feed = ArrayList<Article>()

    init
    {
        ArrayList<Article>().apply {
            add(Article(1,
                    "Test Event 1",
                    DateTime.now().unixMillisLong,
                    "https://images.unsplash.com/photo-1507525428034-b723cf961d3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1353&q=80",
                    "Polsat",
                    "Content Event 1",
                    "-"))
            add(Article(2,
                    "Test Event 2",
                    DateTime.now().unixMillisLong,
                    "https://images.unsplash.com/photo-1507525428034-b723cf961d3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1353&q=80",
                    "Polsat",
                    "Content Event 1",
                    "-"))
            add(Article(3,
                    "Test Event 3",
                    DateTime.now().unixMillisLong,
                    "https://images.unsplash.com/photo-1507525428034-b723cf961d3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1353&q=80",
                    "Polsat",
                    "Content Event 1",
                    "-"))
        }
    }

    override val feedFlow: Flow<List<Article>>
        get() = flowOf(feed)

    override fun getArticle(id: Int): Flow<Article> = flow {
        emit(feed.first { it.id == id })
    }

    override suspend fun fetchFeed(countryCode: String)
    {
    }
}