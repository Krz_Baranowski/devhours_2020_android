package krzbb.com.androidbasics_navigation.feed.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soywiz.klock.DateTime

@Entity
data class Article(
        @PrimaryKey(autoGenerate = true)
        val id: Int = 0,
        val title: String,
        val dateUnixMillis: Long,
        val imageUrl: String,
        val author: String,
        val articleUrl: String,
        val content: String
)