package krzbb.com.androidbasics_navigation.feed.data.datasource;

import krzbb.com.androidbasics_navigation.feed.data.FeedModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface FeedApi
{
    @GET("top-headlines")
    suspend fun getTopHeadlines(
            @Query("country") country: String,
            @Query("apiKey") apiKey: String): Response<FeedModel>
}