package krzbb.com.androidbasics_navigation.feed.ui

import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.palette.graphics.Palette
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.soywiz.klock.DateFormat
import com.soywiz.klock.DateTime
import kotlinx.android.synthetic.main.fragment_feed.*
import krzbb.com.androidbasics_navigation.R
import krzbb.com.androidbasics_navigation.databinding.ItemFeedBinding
import krzbb.com.androidbasics_navigation.feed.domain.model.Article
import me.ibrahimyilmaz.kiel.adapterOf
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder
import org.koin.androidx.viewmodel.ext.android.viewModel


class FeedFragment : Fragment()
{
    private val feedViewModel by viewModel<FeedViewModel>()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View?
    {
        return inflater.inflate(R.layout.fragment_feed, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?)
    {
        super.onActivityCreated(savedInstanceState)

        val recyclerViewAdapter = adapterOf<Article> {
            register(
                    layoutResource = R.layout.item_feed,
                    viewHolder = ::FeedItemViewHolder,
                    onBindViewHolder = { vh, _, item ->
                        Glide.with(requireContext())
                                .load(item.imageUrl)
                                .centerCrop()
                                .placeholder(R.drawable.placeholder)
                                .addListener(object : RequestListener<Drawable>
                                {
                                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean = false

                                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean
                                    {
                                        resource?.let {
                                            updateItemTitleBackgroundTint((it as BitmapDrawable).bitmap, vh)
                                        }

                                        return false
                                    }
                                })
                                .into(vh.binding.image)


                        vh.binding.articleCard.setOnClickListener {
                            findNavController().navigate(FeedFragmentDirections.actionPhotosFragmentToArticleDetailsFragment(item.id))
                        }

                        vh.binding.date.text = DateFormat("yyyy-MM-dd, HH:mm").format(DateTime.fromUnix(item.dateUnixMillis).local)
                        vh.binding.title.text = item.title
                    }
            )
        }

        feed.adapter = recyclerViewAdapter

        feedViewModel.feed.observe(viewLifecycleOwner, Observer { items ->
            recyclerViewAdapter.submitList(items)
        })

        fab.setOnClickListener {
            findNavController().navigate(FeedFragmentDirections.actionFeedToFeedCustomizationFragment())
        }
    }

    private fun updateItemTitleBackgroundTint(bitmap: Bitmap, vh: FeedItemViewHolder)
    {
        val builder = Palette.Builder(bitmap)

        val palette = builder.generate { palette: Palette? ->
            palette?.let {
                vh.binding.titleBackground.backgroundTintList = ColorStateList.valueOf(it.getVibrantColor(0))
            }
        }
    }
}

class FeedItemViewHolder(view: View) : RecyclerViewHolder<Article>(view)
{
    val binding = ItemFeedBinding.bind(view)
}