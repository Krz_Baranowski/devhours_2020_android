package krzbb.com.androidbasics_navigation.feed.ui

import androidx.lifecycle.*
import kotlinx.coroutines.launch
import krzbb.com.androidbasics_navigation.feed.domain.FeedRepository
import krzbb.com.androidbasics_navigation.infrastructure.livedata.SingleLiveEvent

class FeedViewModel(private val feedRepository: FeedRepository) : ViewModel()
{
    val feed = feedRepository.feedFlow.asLiveData()

    init
    {
        refreshEvents("pl")
    }

    fun refreshEvents(countryCode: String)
    {
        viewModelScope.launch {
            feedRepository.fetchFeed(countryCode)
        }
    }
}