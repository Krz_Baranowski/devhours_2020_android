package krzbb.com.androidbasics_navigation.feed.domain

import kotlinx.coroutines.flow.Flow
import krzbb.com.androidbasics_navigation.feed.domain.model.Article

interface FeedRepository
{
    suspend fun fetchFeed(countryCode: String)

    fun getArticle(id: Int): Flow<Article>

    val feedFlow: Flow<List<Article>>
}

