package krzbb.com.androidbasics_navigation.feed.domain

import android.app.Application
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingSource
import com.soywiz.klock.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withContext
import krzbb.com.androidbasics_navigation.feed.data.datasource.FeedApi
import krzbb.com.androidbasics_navigation.feed.domain.db.FeedDao
import krzbb.com.androidbasics_navigation.feed.domain.model.Article
import krzbb.com.androidbasics_navigation.infrastructure.const.AppConstans
import krzbb.com.androidbasics_navigation.infrastructure.db.AppDatabase
import java.lang.Exception

class FeedRepositoryImpl(private val application: Application,
                         private val feedApi: FeedApi) : FeedRepository
{
    private val feedDao: FeedDao

    init
    {
        val db = AppDatabase.getInstance(application)
        feedDao = db.feedDao()
    }

    override fun getArticle(id: Int) = feedDao.getArticle(id)

    override val feedFlow: Flow<List<Article>>
        get() = feedDao.loadAllArticlesFlow()
                .flowOn(Dispatchers.Default)

    override suspend fun fetchFeed(countryCode: String) = withContext(Dispatchers.Default)
    {
        val feedResponse = feedApi.getTopHeadlines(countryCode, AppConstans.ApiKey)

        if (feedResponse.isSuccessful)
        {
            val body = feedResponse.body()
            body?.let {
                val mappedFeed = it.articles.map {

                    val dateTime = DateFormat.FORMAT1.parse(it.publishedAt)

                    Article(title = it.title,
                            author = it.author ?: "",
                            content = it.content ?: "",
                            dateUnixMillis = dateTime.local.unixMillisLong,
                            imageUrl = it.urlToImage ?: "",
                            articleUrl = it.url ?: "")
                }

                feedDao.saveAll(mappedFeed)
            }
        }
    }
}

