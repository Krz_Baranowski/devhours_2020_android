package krzbb.com.androidbasics_navigation.feed.domain.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import krzbb.com.androidbasics_navigation.feed.domain.model.Article

@Dao
interface FeedDao
{
    @Query("SELECT * FROM article WHERE id = :articleId")
    fun getArticle(articleId: Int): Flow<Article>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveAll(articles: List<Article>)
    {
        clear()
        insertAll(articles)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(articles: List<Article>)

    @Query("SELECT * FROM article ORDER BY dateUnixMillis DESC")
    fun loadAllArticlesFlow(): Flow<List<Article>>

    @Query("DELETE FROM article")
    suspend fun clear()
}
