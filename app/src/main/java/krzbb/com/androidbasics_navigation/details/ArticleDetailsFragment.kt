package krzbb.com.androidbasics_navigation.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import krzbb.com.androidbasics_navigation.R
import krzbb.com.androidbasics_navigation.databinding.FragmentArticleDetailsBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class ArticleDetailsFragment : Fragment()
{
    private lateinit var binding: FragmentArticleDetailsBinding
    private val articleViewModel by viewModel<ArticleViewModel>()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?): View?
    {
        binding = DataBindingUtil.inflate<FragmentArticleDetailsBinding>(inflater, R.layout.fragment_article_details, container, false).apply {
            viewModel = articleViewModel
            lifecycleOwner = this@ArticleDetailsFragment.viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            val navData = ArticleDetailsFragmentArgs.fromBundle(it)
            articleViewModel.setArticleId(navData.articleId)
        }
    }
}