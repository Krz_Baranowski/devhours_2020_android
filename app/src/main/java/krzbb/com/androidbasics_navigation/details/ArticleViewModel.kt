package krzbb.com.androidbasics_navigation.details

import androidx.lifecycle.*
import com.soywiz.klock.DateFormat
import com.soywiz.klock.DateTime
import krzbb.com.androidbasics_navigation.feed.domain.FeedRepository

class ArticleViewModel(private val feedRepository: FeedRepository) : ViewModel()
{
    val articleId = MutableLiveData<Int>()
    val articleDate = MutableLiveData<String>()

    val article = articleId.switchMap { id ->
        feedRepository.getArticle(id).asLiveData().map { article ->

            articleDate.postValue(DateFormat("yyyy-MM-dd, HH:mm").format(DateTime.fromUnix(article.dateUnixMillis).local))
            article
        }
    }

    fun setArticleId(id: Int)
    {
        articleId.postValue(id)
    }
}
