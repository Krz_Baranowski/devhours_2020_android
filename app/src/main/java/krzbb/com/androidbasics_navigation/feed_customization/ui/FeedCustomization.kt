package krzbb.com.androidbasics_navigation.feed_customization.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import krzbb.com.androidbasics_navigation.R

class FeedCustomizationFragment : BottomSheetDialogFragment()
{
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        return inflater.inflate(R.layout.fragment_feed_customization, container, false)
    }

    override fun getTheme(): Int = R.style.BottomSheetDialogStyle
}